<?php

namespace Tests;

use Ensi\LaravelTestFactories\WithFakerProviderTestCase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFakerProviderTestCase;
}
