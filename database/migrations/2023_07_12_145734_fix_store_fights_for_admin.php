<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909];
    }
};
