<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [2201, 2202, 2203, 2204];
    }
};
