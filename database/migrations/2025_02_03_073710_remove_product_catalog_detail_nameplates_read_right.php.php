<?php

use Database\Helpers\RevokeRightFromAllRolesMigration;

return new class () extends RevokeRightFromAllRolesMigration {
    protected function rights(): array
    {
        return [417];
    }
};
