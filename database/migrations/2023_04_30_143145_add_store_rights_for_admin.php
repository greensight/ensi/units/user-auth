<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709];
    }
};
