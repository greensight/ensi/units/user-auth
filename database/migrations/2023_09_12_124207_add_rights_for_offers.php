<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [2801, 2802, 2803, 2804, 2805, 2806, 2807, 2808];
    }
};
