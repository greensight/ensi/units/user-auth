<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::roles() as $role) {
            DB::table('roles')->updateOrInsert(['id' => $role['id']], [
                'title' => $role['title'],
                'rights_access' => json_encode($role['rights_access'] ?? []),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public static function roles(): array
    {
        $allRights = [
            101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118,
            201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212,
            301, 302, 303, 304, 305,
            401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413,
            501, 502, 503, 504, 505, 506, 507, 508,
            601, 602, 603, 604, 605, 606, 607,
            701, 702, 703, 704, 705, 706, 707,
            801, 802, 803, 804, 805, 806,
            901, 902, 903, 904, 905, 906,
        ];

        return [
            [
                'id' => 101,
                'title' => 'admin',
                'rights_access' => $allRights,
            ],
            [
                'id' => 102,
                'title' => 'super_admin',
                'rights_access' => $allRights,
            ],
            [
                'id' => 115,
                'title' => 'seller_administrator',
            ],
            [
                'id' => 116,
                'title' => 'seo_manager',
            ],
            [
                'id' => 120,
                'title' => 'oms_shift_manager',
            ],
            [
                'id' => 119,
                'title' => 'oms_order_collector',
            ],
            [
                'id' => 111,
                'title' => 'oms_order_picker',
            ],
            [
                'id' => 113,
                'title' => 'oms_call_center',
            ],
            [
                'id' => 201,
                'title' => 'sas_seller_operator',
            ],
            [
                'id' => 202,
                'title' => 'sas_seller_admin',
            ],
            [
                'id' => 114,
                'title' => 'oms_store_employee',
            ],
            [
                'id' => 112,
                'title' => 'oms_logistic',
            ],
            [
                'id' => 106,
                'title' => 'pim_manager',
            ],
            [
                'id' => 103,
                'title' => 'manager_seller',
            ],
            [
                'id' => 104,
                'title' => 'manager_client',
            ],
        ];
    }
};
