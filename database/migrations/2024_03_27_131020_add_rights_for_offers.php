<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [102];
    }

    protected function rights(): array
    {
        return [2808, 2809];
    }
};
