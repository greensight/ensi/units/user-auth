<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [
            2101, 2102, 2103, 2104, 2105,
        ];
    }
};
