<?php

namespace Database\Helpers;

use DB;

abstract class RevokeRightFromAllRolesMigration extends RevokeRightsMigration
{
    protected function roles(): array
    {
        return DB::table('roles')->pluck('id')->toArray();
    }
}
