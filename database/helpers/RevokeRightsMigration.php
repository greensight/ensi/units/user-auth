<?php

namespace Database\Helpers;

abstract class RevokeRightsMigration extends RightsMigration
{
    public function up()
    {
        $this->revoke();
    }

    public function down()
    {
        $this->assign();
    }
}
