<?php

namespace App\Domain\Kafka\Messages\Send\Events;

use App\Domain\Kafka\Messages\Send\KafkaMessage;

class DeactivatedUserMessage extends KafkaMessage
{
    public function __construct(protected int $userId, protected string $causeDeactivation)
    {
    }

    public function toArray(): array
    {
        return [
            'user_id' => $this->userId,
            'cause_deactivation' => $this->causeDeactivation,
        ];
    }

    public function topicKey(): string
    {
        return 'deactivated-user';
    }
}
