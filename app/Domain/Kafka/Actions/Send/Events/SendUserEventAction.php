<?php

namespace App\Domain\Kafka\Actions\Send\Events;

use App\Domain\Kafka\Actions\Send\SendKafkaMessageAction;
use App\Domain\Kafka\Messages\Send\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvents\UserPayload;
use App\Domain\Users\Models\User;

class SendUserEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(User $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new UserPayload($model), $event, 'users');
        $this->sendAction->execute($modelEvent);
    }
}
