<?php

namespace App\Domain\Auth\Actions;

use App\Domain\Users\Models\User;
use League\OAuth2\Server\Exception\OAuthServerException;

class LogoutIfAccessDenied
{
    public function __construct(protected readonly DeleteAuthorisationDataAction $deleteAuthorisationDataAction)
    {
    }

    public function execute(User $user): void
    {
        try {
            $user->checkAccess();
        } catch (OAuthServerException $e) {
            $this->deleteAuthorisationDataAction->execute($user);

            throw $e;
        }
    }
}
