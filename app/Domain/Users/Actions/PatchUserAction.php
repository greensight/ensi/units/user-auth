<?php

namespace App\Domain\Users\Actions;

use App\Domain\Kafka\Actions\Send\SendDeactivatedUserAction;
use App\Domain\Users\Models\User;

class PatchUserAction
{
    public function __construct(protected readonly SendDeactivatedUserAction $sendDeactivatedUserAction)
    {
    }

    public function execute(int $id, array $fields): User
    {
        /** @var User $user */
        $user = User::query()->findOrFail($id);
        $user->update($fields);

        if ($user->isDirty('active') && !$user->active) {
            $this->sendDeactivatedUserAction->execute($user->id, $fields['cause_deactivation']);
        }

        return $user;
    }
}
