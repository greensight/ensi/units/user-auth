<?php

namespace App\Domain\Users\Actions;

use App\Domain\Kafka\Actions\Send\SendDeactivatedUserAction;
use App\Domain\Users\Actions\Data\MassChangeActiveData;
use App\Domain\Users\Models\User;

class MassChangeActiveAction
{
    public function __construct(protected readonly SendDeactivatedUserAction $sendDeactivatedUserAction)
    {
    }

    public function execute(MassChangeActiveData $data): void
    {
        User::query()
            ->whereIn('id', $data->userIds)
            ->update(['active' => $data->active]);

        if (!$data->active) {
            foreach ($data->userIds as $userId) {
                $this->sendDeactivatedUserAction->execute($userId, $data->causeDeactivation);
            }
        }
    }
}
