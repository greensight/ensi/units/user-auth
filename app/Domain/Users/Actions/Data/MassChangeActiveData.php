<?php

namespace App\Domain\Users\Actions\Data;

class MassChangeActiveData
{
    public function __construct(
        public array $userIds,
        public bool $active,
        public ?string $causeDeactivation,
    ) {
    }
}
