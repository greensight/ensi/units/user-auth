<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;

class DeactivatePasswordTokenAction
{
    public function execute(): void
    {
        $usersWithOldPasswordToken = User::query()
            ->where('password_token_created_at', '<', now()->subDays(User::LIFE_TIME_PASSWORD_TOKEN))
            ->get();
        if (!$usersWithOldPasswordToken->isEmpty()) {
            $usersWithOldPasswordToken->each(function (User $user) {
                $user->destroyPasswordToken();
                $user->save();
            });
        }
    }
}
