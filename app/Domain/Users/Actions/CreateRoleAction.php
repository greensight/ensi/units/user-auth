<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\Role;

class CreateRoleAction
{
    public function execute(array $fields): Role
    {
        $role = new Role();
        $role->fill($fields);
        $role->save();

        return $role;
    }
}
