<?php

namespace App\Domain\Users\Models;

use App\Http\ApiV1\OpenApiGenerated\Enums\RightsAccessEnum;

class RightsAccess
{
    public const ORDERS = 'Список заказов';
    public const REFUNDS = 'Возвраты';
    public const SETTINGS = 'Настройки заказов';
    public const PRODUCT_CATALOG = 'Каталог товаров';
    public const PRODUCT_CATEGORIES = 'Категории товаров';
    public const PRODUCT_ATTRIBUTES = 'Атрибуты товаров';
    public const USERS = 'Пользователи';
    public const ROLES = 'Роли';
    public const PAGES = 'Информационные страницы';
    public const DISCOUNTS = 'Скидки';
    public const PROMO_CODES = 'Промокоды';
    public const BRANDS = 'Бренды';
    public const PRODUCT_GROUPS = 'Товарные склейки';
    public const CUSTOMERS = 'Клиенты';
    public const BANNERS = 'Баннеры';
    public const REVIEWS = 'Отзывы';
    public const NAMEPLATES = 'Теги';
    public const SEO = 'SEO';
    public const IMPORTS = 'Импорт товаров';
    public const STORES = 'Склады';
    public const FEED_SETTINGS = 'Настройки фидов';
    public const SELLERS = 'Продавцы';
    public const DELIVERY_PRICES = 'Тарифы доставки';
    public const DELIVERY_SERVICES = 'Службы доставки';
    public const NOTIFICATION_SETTINGS = 'Настройки уведомлений';
    public const CLOUD_INTEGRATION = 'Параметры поиска';
    public const OFFERS = 'Офферы';
    public const DELIVERY_OPTIONS_SETTINGS = 'Настройки параметров доставки';
    public const POINTS = 'ПВЗ';
    public const PRODUCT_STATUSES = 'Статусы продукта';
    public const SELLER_USERS = 'Пользователи продавцов';
    public const TECHNICAL_TABLES = 'Технические таблицы';

    public function __construct(
        public readonly RightsAccessEnum $id,
        public readonly string $title
    ) {
    }

    public static function all(): array
    {
        return [
            ['section' => self::ORDERS, 'items' => self::orders()],
            ['section' => self::REFUNDS, 'items' => self::refunds()],
            ['section' => self::SETTINGS, 'items' => self::settings()],
            ['section' => self::PRODUCT_CATALOG, 'items' => self::productCatalog()],
            ['section' => self::PRODUCT_CATEGORIES, 'items' => self::productCategories()],
            ['section' => self::PRODUCT_ATTRIBUTES, 'items' => self::productAttributes()],
            ['section' => self::USERS, 'items' => self::users()],
            ['section' => self::ROLES, 'items' => self::roles()],
            ['section' => self::PAGES, 'items' => self::pages()],
            ['section' => self::DISCOUNTS, 'items' => self::discounts()],
            ['section' => self::PROMO_CODES, 'items' => self::promoCodes()],
            ['section' => self::BRANDS, 'items' => self::brands()],
            ['section' => self::PRODUCT_GROUPS, 'items' => self::productGroups()],
            ['section' => self::CUSTOMERS, 'items' => self::customers()],
            ['section' => self::BANNERS, 'items' => self::banners()],
            ['section' => self::REVIEWS, 'items' => self::reviews()],
            ['section' => self::NAMEPLATES, 'items' => self::nameplates()],
            ['section' => self::SEO, 'items' => self::seo()],
            ['section' => self::SELLERS, 'items' => self::sellers()],
            ['section' => self::STORES, 'items' => self::stores()],
            ['section' => self::FEED_SETTINGS, 'items' => self::feedSettings()],
            ['section' => self::IMPORTS, 'items' => self::imports()],
            ['section' => self::DELIVERY_PRICES, 'items' => self::deliveryPrices()],
            ['section' => self::DELIVERY_SERVICES, 'items' => self::deliveryServices()],
            ['section' => self::NOTIFICATION_SETTINGS, 'items' => self::notificationSettings()],
            ['section' => self::CLOUD_INTEGRATION, 'items' => self::cloudIntegration()],
            ['section' => self::OFFERS, 'items' => self::offers()],
            ['section' => self::DELIVERY_OPTIONS_SETTINGS, 'items' => self::deliveryOptionsSettings()],
            ['section' => self::POINTS, 'items' => self::points()],
            ['section' => self::PRODUCT_STATUSES, 'items' => self::productStatuses()],
            ['section' => self::SELLER_USERS, 'items' => self::sellerUsers()],
            ['section' => self::TECHNICAL_TABLES, 'items' => self::technicalTables()],
        ];
    }

    public static function orders(): array
    {
        return [
            new static(
                RightsAccessEnum::ORDER_LIST_READ,
                'Пользователю доступен табличный список с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_READ,
                'Пользователю доступна детальная страница заказа со всеми со всеми вкладками и данными для просмотра'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_MAIN_READ,
                'Пользователю доступна вкладка "Главное" для просмотра'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_CLIENT_READ,
                'Пользователю доступна вкладка "Клиент" для просмотра'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_PRODUCTS_READ,
                'Пользователю доступна вкладка "Товары" для просмотра'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_DELIVERY_READ,
                'Пользователю доступна вкладка "Доставка" для просмотра'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_COMMENTS_READ,
                'Пользователю доступна вкладка "Комментарии" для просмотра'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_READ,
                'Пользователю доступна вкладка "Вложения" для просмотра'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_EDIT,
                'Пользователю доступно только редактирование данных заказа (без действий)'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_COMMENTS_EDIT,
                'Пользователю доступна функция редактирование комментариев во вкладке "Комментарии" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_PROBLEMATIC_EDIT,
                'Пользователю доступна функция добавления/удаления проблемы к заказу и комментария  к проблеме во вкладке "Комментариии" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_DELIVERY_EDIT,
                'Пользователю доступна функция редактирования способа доставки во вкладке "Доставка" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_DELIVERY_COMMENTS_EDIT,
                'Пользователю доступна функция редактирования комментария во вкладке "Доставка" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_CLIENT_EDIT,
                'Пользователю доступна функция редактирования полей клиента во вкладке "Клиент" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_WRITE,
                'Пользователю доступна функция загрузка вложений во вкладке "Вложения" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_DELETE,
                'Пользователю доступна функция удаления вложений во вкладке "Вложения" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_STATUS_EDIT,
                'Пользователю доступно изменение статуса заказа на детальной странице + кнопка "Действия"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_REFUND_CREATE,
                'Пользователю доступно создание заявки на возврат + кнопка "Действия"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_PRODUCTS_EDIT,
                'Пользователю доступно редактирование списка товаров в заказе"'
            ),
            new static(
                RightsAccessEnum::ORDER_DETAIL_SHIPMENT_EDIT,
                'Пользователю доступно изменение статуса отгрузки на детальной странице заказ'
            ),
        ];
    }

    public static function refunds(): array
    {
        return [
            new static(
                RightsAccessEnum::REFUND_LIST_READ,
                'Пользователю доступен табличный список с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_READ,
                'Пользователю доступна детальная страница возврата'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_MAIN_READ,
                'Пользователю доступна вкладка "Главное" для просмотра'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_PRODUCTS_READ,
                'Пользователю доступна вкладка "Товары" для просмотра'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_ATTACHMENTS_READ,
                'Пользователю доступна вкладка "Вложения" для просмотра'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_EDIT,
                'Пользователю доступно только редактирование данных возврата (без действий)'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_RESPONSIBLE_EDIT,
                'Пользователю доступна функция изменение ответственного за возврат во вкладке "Главное" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_ATTACHMENTS_WRITE,
                'Пользователю доступна функция загрузка вложений  во вкладке "Вложения" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_ATTACHMENTS_DELETE,
                'Пользователю доступна функция удаления вложений  во вкладке "Вложения" + кнопки "Сохранить"/"Отменить"'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_REASONS_READ,
                'Пользователю доступен табличный список с причинами возврата по кнопке"Настройка причин возврата"'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_REASONS_CREATE,
                'Пользователю доступно создание причин возврата + кнопка "Добавить причину"'
            ),
            new static(
                RightsAccessEnum::REFUND_DETAIL_REASONS_EDIT,
                'Пользователю доступно редактирование причин возврата + кнопка "Изменить причину"'
            ),
        ];
    }

    public static function settings(): array
    {
        return [
            new static(
                RightsAccessEnum::SETTING_LIST_READ,
                'Пользователю доступна для просмотра страница настроек'
            ),
            new static(
                RightsAccessEnum::SETTING_BASKET_EDIT,
                'Параметры корзины. Пользователю доступно редактирование параметра + кнопка "Изменить параметр"'
            ),
            new static(
                RightsAccessEnum::SETTING_PAYMENT_TIME_EDIT,
                'Срок оплаты заказа. Пользователю доступно редактирование параметра + кнопка "Изменить параметр"'
            ),
            new static(
                RightsAccessEnum::SETTING_ORDER_CREATE_TIME_EDIT,
                'Срок оформления заказа. Пользователю доступно редактирование параметра + кнопка "Изменить параметр"'
            ),
            new static(
                RightsAccessEnum::SETTING_ORDER_PROCESS_TIME_EDIT,
                'Максимальное время обработки. Пользователю доступно редактирование параметра + кнопка "Изменить параметр"'
            ),
        ];
    }

    public static function productCatalog(): array
    {
        return [
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_LIST_READ,
                'Пользователю доступен табличный список с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_READ,
                'Пользователю доступна детальная страница товара со всеми со всеми вкладками и данными для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_PRODUCT_CREATE,
                'Пользователь может создавать товар'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
                'Пользователю доступна функция редактирование любых данных товара и совершения любых действий с ним'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_READ,
                'Пользователю доступна вкладка "Основные данные" для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_READ,
                'Пользователю доступна вкладка "Контент" для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_PRODUCT_GROUPS_READ,
                'Пользователю доступна вкладка "Товарные склейки" для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_READ,
                'Пользователю доступна вкладка "Характеристики" для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_ACTIVE_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_EDIT,
                'Пользователю доступно полное редактирование во вкладке "Контент"'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_EDIT,
                'Пользователю доступно полное редактирование во вкладке "Характеристики"'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_EDIT,
                'Пользователю доступно полное редактирование во вкладке "Основные данные"'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_PRODUCT_DELETE,
                'Пользователь может удалять товары'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATALOG_MASS_PATCH,
                'Пользователю доступна функция "Изменить атрибуты" в табличном списке товаров',
            ),
            new static(
                RightsAccessEnum::CATALOG_CACHE_ENTITIES_MIGRATION,
                'Пользователю доступна миграция состояния сущностей, из которых наполняется индекс'
            ),
        ];
    }

    public static function productCategories(): array
    {
        return [
            new static(
                RightsAccessEnum::PRODUCT_CATEGORY_LIST_READ,
                'Пользователю доступен табличный список с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_READ,
                'Пользователю доступна детальная страница категории со всеми вкладками и данными для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_WRITE,
                'Пользователю доступна функция редактирование любых данных категории и совершения любых действий с ним'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATEGORY_CREATE,
                'Пользователь может создавать категории'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_ATTRIBUTES_WRITE,
                'Пользователь может добавлять и редактировать атрибуты категории'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_EDIT,
                'Пользователь может редактирование данные о категории (кроме активности)'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATEGORY_DELETE,
                'Пользователь может удалять категории'
            ),
            new static(
                RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_ACTIVE_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
        ];
    }

    public static function productAttributes(): array
    {
        return [
            new static(
                RightsAccessEnum::PRODUCT_ATTRIBUTES_LIST_READ,
                'Пользователю доступен табличный список с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_READ,
                'Пользователю доступна детальная страница атрибутов со всеми со всеми вкладками и данными для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_EDIT,
                'Пользователю доступно полное редактирование атрибутов, включая активность и отображение на витрине'
            ),
            new static(
                RightsAccessEnum::PRODUCT_ATTRIBUTES_CREATE,
                'Пользователь может создать новый атрибут товара и сохранить его'
            ),
            new static(
                RightsAccessEnum::PRODUCT_ATTRIBUTES_DELETE,
                'Пользователь может удалять атрибутов'
            ),
            new static(
                RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_ACTIVE_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
            new static(
                RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_SHOW_EDIT,
                'Пользователю доступно управление чек-боксом "Отображение на витрине"'
            ),
        ];
    }

    public static function users(): array
    {
        return [
            new static(
                RightsAccessEnum::USER_LIST_READ,
                'Пользователю доступен табличный список с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::USER_DETAIL_READ,
                'Пользователю доступна детальная страница пользователя со всеми со всеми вкладками и данными для просмотра'
            ),
            new static(
                RightsAccessEnum::USER_DETAIL_EDIT,
                'Пользователю доступно полное редактирование пользователя'
            ),
            new static(
                RightsAccessEnum::USER_DETAIL_ACTIVE_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
            new static(
                RightsAccessEnum::USER_DETAIL_PASSWORD_EDIT,
                'Пользователю доступно изменение пароля'
            ),
            new static(
                RightsAccessEnum::USER_DETAIL_ROLE_EDIT,
                'Пользователю доступно управление ролями пользователя'
            ),
            new static(
                RightsAccessEnum::USER_CREATE,
                'Пользователю доступно создание пользователя'
            ),
        ];
    }

    public static function roles(): array
    {
        return [
            new static(
                RightsAccessEnum::ROLE_LIST_READ,
                'Пользователю доступен табличный список с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::ROLE_DETAIL_READ,
                'Пользователю доступна детальная страница роли со всеми со всеми вкладками и данными для просмотра'
            ),
            new static(
                RightsAccessEnum::ROLE_CREATE,
                'Создание ролей'
            ),
            new static(
                RightsAccessEnum::ROLE_DETAIL_EDIT,
                'Пользователю доступно полное редактирование роли'
            ),
            new static(
                RightsAccessEnum::ROLE_DETAIL_ACTIVE_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
            new static(
                RightsAccessEnum::ROLE_DELETE,
                'Пользователю доступно удаление роли'
            ),
        ];
    }

    public static function pages(): array
    {
        return [
            new static(
                RightsAccessEnum::PAGE_LIST_READ,
                'Пользователю доступен табличный список с информационными страницами'
            ),
            new static(
                RightsAccessEnum::PAGE_DETAIL_READ,
                'Пользователю доступна детальная страница информационной страницы со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::PAGE_DETAIL_EDIT,
                'Пользователю доступно редактирование информационной страницы, включая управление активностью'
            ),
            new static(
                RightsAccessEnum::PAGE_DETAIL_ACTIVE_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
            new static(
                RightsAccessEnum::PAGE_CREATE,
                'Пользователю доступно создание информационной страницы'
            ),
            new static(
                RightsAccessEnum::PAGE_DELETE,
                'Пользователю доступно удаление информационной страницы'
            ),
        ];
    }

    public static function brands(): array
    {
        return [
            new static(
                RightsAccessEnum::BRAND_LIST_READ,
                'Пользователю доступен табличный список с брендами с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::BRAND_DETAIL_READ,
                'Пользователю доступна детальная страница бренда со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::BRAND_DETAIL_EDIT,
                'Пользователю доступно полное редактирование бренда, включая управление активностью'
            ),
            new static(
                RightsAccessEnum::BRAND_DETAIL_ACTIVE_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
            new static(
                RightsAccessEnum::BRAND_CREATE,
                'Пользователю доступно создание бренда'
            ),
            new static(
                RightsAccessEnum::BRAND_DELETE,
                'Пользователю доступно удаление бренда'
            ),
        ];
    }

    public static function discounts(): array
    {
        return [
            new static(
                RightsAccessEnum::DISCOUNT_LIST_READ,
                'Пользователю доступен табличный список со скидками'
            ),
            new static(
                RightsAccessEnum::DISCOUNT_DETAIL_READ,
                'Пользователю доступна детальная страница скидки со всеми данными для просмотра и без возможности редактирования полей'
            ),
            new static(
                RightsAccessEnum::DISCOUNT_CREATE,
                'Пользователю доступно создание скидки'
            ),
            new static(
                RightsAccessEnum::DISCOUNT_DETAIL_EDIT,
                'Пользователю доступно редактирование скидки'
            ),
            new static(
                RightsAccessEnum::DISCOUNT_DELETE,
                'Пользователю доступно удаление скидки'
            ),
            new static(
                RightsAccessEnum::DISCOUNT_MASS_STATUS_UPDATE,
                'Пользователю доступно массовое обновление статусов скидки в табличном списке'
            ),
        ];
    }

    public static function promoCodes(): array
    {
        return [
            new static(
                RightsAccessEnum::PROMO_CODE_LIST_READ,
                'Пользователю доступен табличный список с промокодами'
            ),
            new static(
                RightsAccessEnum::PROMO_CODE_DETAIL_READ,
                'Пользователю доступна детальная страница промокода со всеми данными для просмотра и без возможности редактирования полей'
            ),
            new static(
                RightsAccessEnum::PROMO_CODE_CREATE,
                'Пользователю доступно создание промокода'
            ),
            new static(
                RightsAccessEnum::PROMO_CODE_DETAIL_EDIT,
                'Пользователю доступно редактирование промокода'
            ),
            new static(
                RightsAccessEnum::PROMO_CODE_DELETE,
                'Пользователю доступно удаление промокода'
            ),
        ];
    }

    public static function productGroups(): array
    {
        return [
            new static(
                RightsAccessEnum::PRODUCT_GROUP_LIST_READ,
                'Пользователю доступен табличный список склеек с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::PRODUCT_GROUP_DETAIL_READ,
                'Пользователю доступна детальная страница склейки со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_GROUP_DETAIL_FIELDS_EDIT,
                'Пользователю доступно редактирование склейки (кроме изменения активности и удаления)'
            ),
            new static(
                RightsAccessEnum::PRODUCT_GROUP_DETAIL_ACTIVE_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
            new static(
                RightsAccessEnum::PRODUCT_GROUP_CREATE,
                'Пользователю доступно создание склейки'
            ),
            new static(
                RightsAccessEnum::PRODUCT_GROUP_DELETE,
                'Пользователю доступно удаление склейки'
            ),
            new static(
                RightsAccessEnum::PRODUCT_GROUP_DETAIL_FULL_EDIT,
                'Пользователю доступно редактирование всех полей товарной склейки'
            ),
        ];
    }

    public static function customers(): array
    {
        return [
            new static(
                RightsAccessEnum::CUSTOMER_DELETE_PERSONAL_DATA,
                'Пользователю доступно удаление персональных данных клиента'
            ),
            new static(
                RightsAccessEnum::CUSTOMER_LIST_READ,
                'Пользователю доступен табличный список клиентов с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::CUSTOMER_DETAIL_READ,
                'Пользователю доступна детальная страница клиента со всеми со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::CUSTOMER_DETAIL_EDIT,
                'Пользователю доступно редактирование данных клиента'
            ),
        ];
    }

    public static function banners(): array
    {
        return [
            new static(
                RightsAccessEnum::BANNER_LIST_READ,
                'Пользователю доступен табличный список с баннерами с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::BANNER_DETAIL_READ,
                'Пользователю доступна детальная страница баннера со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::BANNER_DETAIL_EDIT,
                'Пользователю доступно редактирование баннера'
            ),
            new static(
                RightsAccessEnum::BANNER_CREATE,
                'Пользователю доступно создание баннера'
            ),
            new static(
                RightsAccessEnum::BANNER_DELETE,
                'Пользователю доступно удаление баннера'
            ),
        ];
    }

    public static function reviews(): array
    {
        return [
            new static(
                RightsAccessEnum::REVIEW_LIST_READ,
                'Пользователю доступен табличный список с отзывами с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::REVIEW_DETAIL_READ,
                'Пользователю доступна детальная страница отзывами со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::REVIEW_DELETE,
                'Пользователю доступно удаление отзывов'
            ),
            new static(
                RightsAccessEnum::REVIEW_DETAIL_EDIT,
                'Пользователю доступно изменение статуса отзыва'
            ),
        ];
    }

    public static function nameplates(): array
    {
        return [
            new static(
                RightsAccessEnum::NAMEPLATE_LIST_READ,
                'Пользователю доступен табличный список тегов с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::NAMEPLATE_DETAIL_READ,
                'Пользователю доступна детальная страница товарного тега со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::NAMEPLATE_DETAIL_EDIT,
                'Пользователю доступно редактирование товарного тега'
            ),
            new static(
                RightsAccessEnum::NAMEPLATE_CREATE,
                'Пользователю доступно создание товарного тега'
            ),
            new static(
                RightsAccessEnum::NAMEPLATE_DELETE,
                'Пользователю доступно удаление товарного тега'
            ),
        ];
    }

    public static function seo(): array
    {
        return [
            new static(
                RightsAccessEnum::SEO_TEMPLATE_LIST_READ,
                'Пользователю доступен табличный список SEO-шаблонов с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::SEO_TEMPLATE_DETAIL_READ,
                'Пользователю доступна детальная страница SEO-шаблона со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::SEO_TEMPLATE_DETAIL_EDIT,
                'Пользователю доступно редактирование SEO-шаблона'
            ),
            new static(
                RightsAccessEnum::SEO_TEMPLATE_CREATE,
                'Пользователю доступно создание SEO-шаблона'
            ),
            new static(
                RightsAccessEnum::SEO_TEMPLATE_DELETE,
                'Пользователю доступно удаление SEO-шаблона'
            ),
        ];
    }

    public static function stores(): array
    {
        return [
            new static(
                RightsAccessEnum::STORE_LIST_READ,
                'Пользователю доступен табличный список складов с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::STORE_DETAIL_READ,
                'Пользователю доступна детальная страница склада со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::STORE_MAIN_DATA_EDIT,
                'Пользователю доступно полное редактирование во вкладке "Основные данные"'
            ),
            new static(
                RightsAccessEnum::STORE_WORKING_EDIT,
                'Пользователю доступно полное редактирование во вкладке "Время работы"'
            ),
            new static(
                RightsAccessEnum::STORE_CONTACTS_EDIT,
                'Пользователю доступно полное редактирование во вкладке "Контакты"'
            ),
            new static(
                RightsAccessEnum::STORE_PICKUP_TIME_EDIT,
                'Пользователю доступно полное редактирование во вкладке "График отгрузки"'
            ),
            new static(
                RightsAccessEnum::STORE_ACTIVITY_EDIT,
                'Пользователю доступно управление чек-боксом "Активность"'
            ),
            new static(
                RightsAccessEnum::STORE_CREATE,
                'Пользователь может создавать склад'
            ),
        ];
    }

    public static function feedSettings(): array
    {
        return [
            new static(
                RightsAccessEnum::FEED_SETTINGS_LIST_READ,
                'Пользователю доступен табличный список с настройками фидов с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::FEED_SETTINGS_DETAIL_READ,
                'Пользователю доступна детальная страница настройками фида со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::FEED_SETTINGS_CREATE,
                'Пользователю доступно создание настроек фида'
            ),
            new static(
                RightsAccessEnum::FEED_SETTINGS_DETAIL_EDIT,
                'Пользователю доступно редактирование настроек фида'
            ),
            new static(
                RightsAccessEnum::FEED_ENTITIES_MIGRATION,
                'Пользователю доступна миграция состояния сущностей, из которых формируются фиды'
            ),
        ];
    }

    public static function imports(): array
    {
        return [
            new static(
                RightsAccessEnum::IMPORT_CREATE,
                'Пользователю доступен импорт товаров'
            ),
            new static(
                RightsAccessEnum::IMPORT_LIST_READ,
                'Пользователю доступен просмотр журнала импорта товаров'
            ),
            new static(
                RightsAccessEnum::IMPORT_WARNING_LIST_READ,
                'Пользователю доступен просмотр журнала ошибок импорта товаров'
            ),
        ];
    }

    public static function sellers(): array
    {
        return [
            new static(
                RightsAccessEnum::SELLER_LIST_READ,
                'Пользователю доступен табличный список продавцов с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::SELLER_DETAIL_READ,
                'Пользователю доступна детальная страница продавца со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::SELLER_CREATE,
                'Пользователю доступно создание продавца'
            ),
            new static(
                RightsAccessEnum::SELLER_DETAIL_EDIT,
                'Пользователю доступно редактирование продавца'
            ),
        ];
    }

    private static function deliveryPrices(): array
    {
        return [
            new static(
                RightsAccessEnum::DELIVERY_PRICE_LIST_READ,
                'Пользователю доступен табличный список тарифов с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::DELIVERY_PRICE_DETAIL_READ,
                'Пользователю доступна детальная страница тарифа со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::DELIVERY_PRICE_DETAIL_EDIT,
                'Пользователю доступно редактирование тарифа'
            ),
            new static(
                RightsAccessEnum::DELIVERY_PRICE_CREATE,
                'Пользователю доступно создание тарифа'
            ),
            new static(
                RightsAccessEnum::DELIVERY_PRICE_DELETE,
                'Пользователю доступно удаление тарифа'
            ),
        ];
    }

    private static function deliveryServices(): array
    {
        return [
            new static(
                RightsAccessEnum::DELIVERY_SERVICES_TABLES_READ,
                'Пользователю доступен табличный список служб доставки с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::DELIVERY_SERVICES_DETAIL_READ,
                'Пользователю доступна детальная страница службы доставки со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::DELIVERY_SERVICES_DETAIL_EDIT,
                'Пользователю доступно редактирование службы доставки'
            ),
            new static(
                RightsAccessEnum::DELIVERY_SERVICES_PAYMENT_METHOD_EDIT,
                'Пользователю доступно добавление и удаление доступных способов оплаты из службы доставки'
            ),
        ];
    }

    private static function notificationSettings(): array
    {
        return [
            new static(
                RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ,
                'Пользователю доступен табличный список настроек уведомлений с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::NOTIFICATION_SETTING_DETAIL_READ,
                'Пользователю доступна детальная страница настроек уведомлений со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::NOTIFICATION_SETTING_CREATE,
                'Пользователю доступно создание настроек уведомлений'
            ),
            new static(
                RightsAccessEnum::NOTIFICATION_SETTING_DETAIL_EDIT,
                'Пользователю доступно редактирование настроек уведомлений'
            ),
            new static(
                RightsAccessEnum::NOTIFICATION_SETTING_DELETE,
                'Пользователю доступно удаление настроек уведомлений'
            ),
        ];
    }

    private static function cloudIntegration(): array
    {
        return [
            new static(
                RightsAccessEnum::CLOUD_INTEGRATION_DETAIL_EDIT,
                'Пользователю доступно редактирование подключением Search'
            ),
            new static(
                RightsAccessEnum::CLOUD_INTEGRATION_READ,
                'Пользователю доступен просмотр подключения Search'
            ),
            new static(
                RightsAccessEnum::CLOUD_INTEGRATION_ACTIVITY_EDIT,
                'Пользователю доступно изменение активности подключения Search'
            ),
        ];
    }

    private static function offers(): array
    {
        return [
            new static(
                RightsAccessEnum::OFFER_LIST_READ,
                'Пользователю доступен табличный список офферов с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::OFFER_DETAIL_MAIN_EDIT,
                'Пользователю доступно полное редактирование на вкладке "Основные данные"'
            ),
            new static(
                RightsAccessEnum::OFFER_DETAIL_STOCKS_EDIT,
                'Пользователю доступно полное редактирование на вкладке "Стоки"'
            ),
            new static(
                RightsAccessEnum::OFFER_DETAIL_ALLOW_PUBLISH_EDIT,
                'Пользователю доступно управление чекбоксом "Публикация"'
            ),
            new static(
                RightsAccessEnum::OFFER_DETAIL_PRICE_EDIT,
                'Пользователю доступно управлением полем "Стоимость"'
            ),
            new static(
                RightsAccessEnum::OFFER_DETAIL_EDIT,
                'Пользователю доступна функция редактирование любых данных оффера и совершения любых действий с ним'
            ),
            new static(
                RightsAccessEnum::OFFER_DETAIL_READ,
                'Пользователю доступна детальная страница оффера со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::OFFER_ENTITIES_SYNC,
                'Пользователю доступно обновление данных офферов'
            ),
            new static(
                RightsAccessEnum::OFFER_MIGRATION,
                'Пользователю доступна миграция состояния офферов'
            ),
        ];
    }

    private static function deliveryOptionsSettings(): array
    {
        return [
            new static(
                RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_EDIT,
                'Пользователю доступно редактирование параметров доставки'
            ),
            new static(
                RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_READ,
                'Пользователю доступен просмотр параметров доставки'
            ),
        ];
    }

    private static function points(): array
    {
        return [
            new static(
                RightsAccessEnum::POINT_LIST_READ,
                'Пользователю доступен табличный список ПВЗ с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::POINT_DETAIL_READ,
                'Пользователю доступна детальная страница ПВЗ со всеми данными для просмотра '
            ),
            new static(
                RightsAccessEnum::POINT_DETAIL_MAIN_DATA_EDIT,
                'Пользователю доступно полное редактирование во вкладке "Основные данные"'
            ),
            new static(
                RightsAccessEnum::POINT_DETAIL_ADDRESS_EDIT,
                'Пользователю доступно полное редактирование во вкладке "Адрес"'
            ),
        ];
    }

    private static function productStatuses(): array
    {
        return [
            new static(
                RightsAccessEnum::PRODUCT_STATUS_LIST_READ,
                'Пользователю доступен табличный список статусов продукта с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::PRODUCT_STATUS_DETAIL_READ,
                'Пользователю доступна детальная страница статуса продукта со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::PRODUCT_STATUS_CREATE,
                'Пользователю доступно создание статуса продукта'
            ),
            new static(
                RightsAccessEnum::PRODUCT_STATUS_DETAIL_EDIT,
                'Пользователю доступно редактирование статуса продукта'
            ),
            new static(
                RightsAccessEnum::PRODUCT_STATUS_DELETE,
                'Пользователю доступно удаление статуса продукта'
            ),
        ];
    }

    private static function sellerUsers(): array
    {
        return [
            new static(
                RightsAccessEnum::SELLER_USER_LIST_READ,
                'Пользователю доступен табличный список пользователей продавца с фильтрами и поиском'
            ),
            new static(
                RightsAccessEnum::SELLER_USER_DETAIL_READ,
                'Пользователю доступна детальная страница пользователя продавца со всеми данными для просмотра'
            ),
            new static(
                RightsAccessEnum::SELLER_USER_DETAIL_EDIT,
                'Пользователю доступно редактирование пользователя продавца'
            ),
            new static(
                RightsAccessEnum::SELLER_USER_CREATE,
                'Пользователю доступно создание пользователя продавца'
            ),
            new static(
                RightsAccessEnum::SELLER_USER_ACTIVITY_EDIT,
                'Пользователю доступно изменение активности пользователя продавца'
            ),
            new static(
                RightsAccessEnum::SELLER_USER_PASSWORD_EDIT,
                'Пользователю доступно редактирование пароля пользователя продавца'
            ),
        ];
    }

    private static function technicalTables(): array
    {
        return [
            new static(
                RightsAccessEnum::TECHNICAL_TABLES_READ,
                'Пользователю доступен просмотр технических таблиц'
            ),
            new static(
                RightsAccessEnum::TECHNICAL_TABLES_ALL,
                'Пользователю доступно редактирование технических таблиц + запуск консольных команд'
            ),
        ];
    }
}
