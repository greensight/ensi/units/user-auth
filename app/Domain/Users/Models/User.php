<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Tests\Factories\UserFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use League\OAuth2\Server\Exception\OAuthServerException;

/**
 * @property int $id
 * @property string $login
 * @property string|null $password
 * @property bool $active
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $phone
 * @property string $email
 * @property string $timezone
 * @property string|null $password_token
 * @property CarbonInterface|null $password_token_created_at
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read string $full_name
 * @property-read string $short_name
 * @property-read Collection|Role[] $roles
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;

    protected $table = 'users';

    public const LIFE_TIME_PASSWORD_TOKEN = 3;

    protected $fillable = ['login', 'password', 'active', 'first_name', 'last_name', 'middle_name', 'email', 'phone', 'timezone'];

    protected $hidden = ['password'];

    public function roles(): BelongsToMany
    {
        return $this
            ->belongsToMany(Role::class)
            ->where('active', '=', true)
            ->using(UserRole::class)
            ->withPivot('expires')
            ->withTimestamps();
    }

    public function setPasswordAttribute(?string $value): void
    {
        $this->attributes['password'] = $value ? User::encryptPassword($value) : null;
    }

    public static function encryptPassword(string $password): string
    {
        return Hash::make($password);
    }

    public function checkPassword($password): bool
    {
        return Hash::check($password, $this->password);
    }

    public function generatePasswordToken(): void
    {
        $this->password_token = md5($this->login . rand(PHP_INT_MIN, PHP_INT_MAX));
        $this->password_token_created_at = now();
    }

    public function destroyPasswordToken(): void
    {
        $this->password_token = null;
        $this->password_token_created_at = null;
    }

    public function isActive(): bool
    {
        return $this->active == true;
    }

    /**
     * Override the field which is used for username in the Laravel Passport authentication
     *
     * @param string $login
     */
    public function findForPassport(string $login)
    {
        return $this->where('login', $login)->first();
    }

    /**
     * Add a password validation callback for Laravel Passport
     *
     * @param $password
     * @return bool Whether the password is valid
     * @throws OAuthServerException
     */
    public function validateForPassportPasswordGrant($password): bool
    {
        $this->checkAccess();

        return $this->checkPassword($password);
    }

    public function getFullNameAttribute(): string
    {
        $pieces = [];
        if ($this->last_name) {
            $pieces[] = $this->last_name;
        }
        if ($this->first_name) {
            $pieces[] = $this->first_name;
        }
        if ($this->middle_name) {
            $pieces[] = $this->middle_name;
        }

        return implode(' ', $pieces);
    }

    public function getShortNameAttribute(): string
    {
        $pieces = [];
        if ($this->last_name) {
            $pieces[] = $this->last_name;
        }
        if ($this->first_name) {
            $pieces[] = mb_substr($this->first_name, 0, 1) . '.';
        }
        if ($this->middle_name) {
            $pieces[] = mb_substr($this->middle_name, 0, 1) . '.';
        }

        return implode(' ', $pieces);
    }

    public static function factory(): UserFactory
    {
        return UserFactory::new();
    }

    /**
     * @throws OAuthServerException
     */
    public function checkAccess(): void
    {
        if (!$this->isActive()) {
            throw new OAuthServerException('Пользователь не активен', 1000, 'inactive_user');
        }

        $this->loadMissing('roles');

        if ($this->roles->isEmpty()) {
            throw new OAuthServerException('У пользователя нет активных ролей', 1001, 'inactive_roles');
        }
    }
}
