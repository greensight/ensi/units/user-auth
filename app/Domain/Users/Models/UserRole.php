<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Tests\Factories\UserRoleFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 * @property CarbonInterface|null $expires
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read User $user
 */
class UserRole extends Pivot
{
    protected $table = 'role_user';

    protected $casts = [
        'expires' => 'datetime',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public static function factory(): UserRoleFactory
    {
        return UserRoleFactory::new();
    }
}
