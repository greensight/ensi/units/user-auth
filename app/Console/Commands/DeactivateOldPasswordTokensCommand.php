<?php

namespace App\Console\Commands;

use App\Domain\Users\Actions\DeactivatePasswordTokenAction;
use Illuminate\Console\Command;

class DeactivateOldPasswordTokensCommand extends Command
{
    protected $signature = 'passwords:deactivate-token';
    protected $description = 'Деактивация токенов паролей, срок жизни которых истек';

    public function handle(DeactivatePasswordTokenAction $action): void
    {
        $action->execute();
    }
}
