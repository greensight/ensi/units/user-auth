<?php

namespace App\Console\Commands;

use App\Domain\Users\Actions\AddRolesToUserAction;
use App\Domain\Users\Actions\CreateUserAction;
use Illuminate\Console\Command;

class CreateUserWithGivenRoleCommand extends Command
{
    protected $signature = 'user:create-with-role
                            {login : User login}
                            {password : User password}
                            {role=102 : Role ID}';

    protected $description = 'Создание пользователя с заданной ролью(по умолчанию роль равна супер админ)';

    public function handle(CreateUserAction $createUserAction, AddRolesToUserAction $addRolesToUserAction): void
    {
        $userFields = [
            'login' => $this->argument('login'),
            'password' => $this->argument('password'),
            'timezone' => 'Europe/Moscow',
        ];

        $user = $createUserAction->execute($userFields);

        $addRolesToUserAction->execute($user->id, [$this->argument('role')], null);
    }
}
