<?php

use App\Domain\Kafka\Actions\Send\SendKafkaMessageAction;
use App\Domain\Users\Models\Role;
use App\Domain\Users\Models\User;
use App\Domain\Users\Models\UserRole;
use App\Http\ApiV1\Modules\Users\Tests\Factories\UserRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\GrantTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\RoleEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Laravel\Passport\Client;
use Laravel\Passport\Token;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertNotEquals;

use Tests\IntegrationTestCase;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/users 201', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    $userData = UserRequestFactory::new()->make();

    postJson('/api/v1/users', $userData)
        ->assertStatus(201)
        ->assertJsonPath('data.login', $userData['login']);

    assertDatabaseHas(User::class, [
        'login' => $userData['login'],
        'active' => $userData['active'],
    ]);
});

test('POST /api/v1/users 400', function () {
    $userData = UserRequestFactory::new()->make();
    $userData['phone'] = '1234';

    postJson('/api/v1/users', $userData)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('POST /api/v1/users check email', function (string $email, int $status = 201) {
    $userData = UserRequestFactory::new()->make();
    $userData['email'] = $email;

    $response = postJson('/api/v1/users', $userData)
        ->assertStatus($status);

    if ($status == 400) {
        $response->assertJsonPath('errors.0.code', "ValidationError");
    }
})->with([
    ['example@domain.com'],
    ['user.name@domain.net'],
    ['user-name@domain.com'],
    ['user-name12@domain.com'],
    ['user-name@domain.ru'],

    ['user name@domain.com', 400],
    ['user-name@domaincom', 400],
    ['user-name@domain-com', 400],
    ['user-name@[127.0.0.1]', 400],
    ['   @domain.com', 400],
    ['user-name@@domain.com', 400],
    ['user..name@domain.com', 400],
    ['.username@domain.com', 400],
    ['username.@domain.com', 400],
    ['user-name@domain.com-', 400],
    ['user-name@-domain.com', 400],
]);

test('GET /api/v1/users/{id} 200', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;

    getJson("/api/v1/users/$id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id);
});

test('GET /api/v1/users/{id} 404', function () {
    getJson("/api/v1/users/1000")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('PATCH /api/v1/users/{id} 200', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create(['phone' => '+79245643567', 'first_name' => 'Ivan']);
    $id = $user->id;
    $userData = UserRequestFactory::new()->only(['phone'])->make(['phone' => '+79245643777']);

    patchJson("/api/v1/users/$id", $userData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.phone', $userData['phone'])
        ->assertJsonPath('data.first_name', "Ivan");

    assertDatabaseHas(User::class, [
        'id' => $id,
        'phone' => $userData['phone'],
        'first_name' => "Ivan",
    ]);
});

test('PATCH /api/v1/users/{id} 404', function () {
    patchJson('/api/v1/users/1000', UserRequestFactory::new()->make())
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('DELETE /api/v1/users/{id} success', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create(['phone' => '+79245643567', 'first_name' => 'Ivan']);
    $id = $user->id;

    deleteJson("/api/v1/users/$id")->assertStatus(200);

    assertModelMissing($user);
});

test("POST /api/v1/users:search 200", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    $users = User::factory()
        ->count(10)
        ->sequence(
            ['active' => true],
            ['active' => false],
        )
        ->create();
    $lastId = $users->last()->id;

    $requestBody = [
        "filter" => [
            "active" => false,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/users:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $lastId)
        ->assertJsonPath('data.0.active', false);
});

test("POST /api/v1/users:search filter success", function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    $filterValue,
) {
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create($value ? [$fieldKey => $value] : []);
    User::factory()->create(is_bool($value) ? [$fieldKey => !$value] : []);

    postJson("/api/v1/users:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $user->{$fieldKey}),
    ], 'sort' => ['-id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $user->id);
})->with([
    ['id', null, null, null],
    ['login', 'login@mail.ru', null, null],
    ['password_token', 'qwf212fsffswqdc', null, null],
    ['active', true, null, null],
    ['phone', '+79999999999', null, null],
    ['phone', '+79999999999', 'phone_like', "+799"],
    ['email', 'mail@mail.ru', null, null],
    ['email', 'mail@mail.ru', 'email_like', "mail.ru"],
    ['last_name', 'Фамилия', 'full_name', "Фамил"],
    ['last_name', 'Случайная_Фамилия', 'last_name_like', "Случайная_"],
    ['first_name', 'Случайное_Имя', 'first_name_like', "Случайное_"],
    ['middle_name', 'Случайное_Отчество', 'middle_name_like', "Случайное_"],
]);

test("POST /api/v1/users:search filter range date success", function (string $fieldKey) {
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create([$fieldKey => '2022-05-05 11:25:14']);
    User::factory()->create();

    $request = [
        'filter' => [
            "{$fieldKey}_gte" => '2022-05-05T10:00:00.0Z',
            "{$fieldKey}_lte" => '2022-05-05T14:00:00.0Z',
        ],
    ];

    postJson("/api/v1/users:search", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $user->id);
})->with([
    ['created_at'],
    ['updated_at'],
]);

test("POST /api/v1/users:search sort success", function (string $sort) {
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    User::factory()->create();
    postJson("/api/v1/users:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'email', 'phone', 'active', 'login', 'created_at', 'updated_at',
]);

test("POST /api/v1/users:search-one 200", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    $users = User::factory()
        ->count(10)
        ->create();
    /** @var User $user */
    $user = $users->random();
    $userId = $user->id;
    $userLogin = $user->login;

    $requestBody = [
        "filter" => [
            "login" => $userLogin,
        ],
    ];

    postJson("/api/v1/users:search-one", $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $userId)
        ->assertJsonPath('data.login', $userLogin);
});

test("POST /api/v1/users:current 200", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $client = Client::factory()->asPasswordClient()->create();

    $password = 'Test1234!';
    /** @var User $user */
    $user = User::factory()->active()->create(['password' => $password]);
    $role = Role::factory()->active()->create();
    UserRole::factory()->fastCreate($user, $role);

    $requestBody = [
        "grant_type" => GrantTypeEnum::PASSWORD,
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "scope" => [],
        "username" => $user->login,
        "password" => $password,
    ];

    $testResponse = postJson('/api/v1/oauth/token', $requestBody);

    $token = json_decode($testResponse->baseResponse->content())->access_token;

    postJson('/api/v1/users:current', [], ['Authorization' => "Bearer $token"])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $user->id);
});

test("POST /api/v1/users:current check logout if access denied 401", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $client = Client::factory()->asPasswordClient()->create();

    $password = 'Test1234!';
    /** @var User $user */
    $user = User::factory()->active()->create(['password' => $password]);
    $role = Role::factory()->active()->create();
    UserRole::factory()->fastCreate($user, $role);

    $requestBody = [
        "grant_type" => GrantTypeEnum::PASSWORD,
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "scope" => [],
        "username" => $user->login,
        "password" => $password,
    ];

    $testResponse = postJson('/api/v1/oauth/token', $requestBody);

    $data = json_decode($testResponse->baseResponse->content());
    $accessToken = $data->access_token;
    $refreshToken = $data->refresh_token;

    $user->active = false;
    $user->save();

    postJson('/api/v1/users:current', [], ['Authorization' => "Bearer $accessToken"])
        ->assertStatus(401)
        ->assertJsonPath(
            'errors.0.message',
            __('override-error-message.Laravel\Passport\Exceptions\OAuthServerException.1000')
        );

    /** @phpstan-ignore-next-line */
    $tokenId = (new Parser(new JoseEncoder()))->parse($accessToken)->claims()->all()['jti'];

    assertDatabaseHas(Token::class, [
        'id' => $tokenId,
        'revoked' => true,
    ]);

    $requestBody = [
        "grant_type" => GrantTypeEnum::REFRESH_TOKEN,
        "scope" => [],
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "refresh_token" => $refreshToken,
    ];

    postJson('/api/v1/oauth/token', $requestBody)
        ->assertStatus(401);
});

test("POST /api/v1/users/{id}:add-roles 200", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;
    $requestBody = [
        'roles' => [
            RoleEnum::ADMIN->value,
            RoleEnum::PIM_MANAGER->value,
        ],
    ];

    postJson("/api/v1/users/{$id}:add-roles", $requestBody)
        ->assertStatus(200);

    assertDatabaseHas(UserRole::class, [
        'user_id' => $id,
        'role_id' => RoleEnum::ADMIN->value,
    ]);

    assertDatabaseHas(UserRole::class, [
        'user_id' => $id,
        'role_id' => RoleEnum::PIM_MANAGER->value,
    ]);
});

test("POST /api/v1/users/{id}:add-roles 400", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;
    $requestBody = [
        'roles' => [0],
    ];

    postJson("/api/v1/users/{$id}:add-roles", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test("POST /api/v1/users/{id}:delete-role 200", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $role = Role::factory()->active()->create();
    UserRole::factory()->fastCreate($user, $role);
    $id = $user->id;
    /** @var Role $role */
    $role = $user->roles->first();

    $requestBody = [
        'role_id' => $role->id,
    ];
    $userRole = UserRole::query()
        ->where('user_id', $id)
        ->where('role_id', $role->id)
        ->first();

    postJson("/api/v1/users/{$id}:delete-role", $requestBody)
        ->assertStatus(200);

    assertModelMissing($userRole);
});

test("POST /api/v1/users/{id}:delete-role 400", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;

    $requestBody = [
        'role_id' => 0,
    ];

    postJson("/api/v1/users/{$id}:delete-role", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test("POST /api/v1/users/{id}:refresh-password-token 200", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;

    postJson("/api/v1/users/{$id}:refresh-password-token")
        ->assertStatus(200);

    /** @var User $updatedUser */
    $updatedUser = User::findOrFail($id);

    assertNotEquals($user->password_token, $updatedUser->password_token);
});

test("POST /api/v1/users/mass/change-active activate users 200", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $users = User::factory()->active(false)->count(5)->create();

    $requestBody = [
        'user_ids' => $users->pluck('id'),
        'active' => true,
    ];
    postJson("/api/v1/users/mass/change-active", $requestBody)
        ->assertStatus(200);

    assertDatabaseMissing(User::class, [
        'active' => false,
    ]);
});

test("POST /api/v1/users/mass/change-active deactivate users 200", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $users = User::factory()->active()->count(5)->create();

    $requestBody = [
        'user_ids' => $users->pluck('id'),
        'active' => false,
        'cause_deactivation' => 'Some cause',
    ];
    postJson("/api/v1/users/mass/change-active", $requestBody)
        ->assertStatus(200);

    assertDatabaseMissing(User::class, [
        'active' => true,
    ]);
});

test("POST /api/v1/users/mass/change-active 400", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $users = User::factory()->count(5)->create(['active' => true]);

    $requestBody = [
        'user_ids' => $users->pluck('id'),
        'active' => false,
    ];
    postJson("/api/v1/users/mass/change-active", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});
