<?php

namespace App\Http\ApiV1\Modules\Users\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class UserRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'login' => $this->faker->unique()->userName(),
            'password' => $this->faker->password(),
            'last_name' => $this->faker->lastName(),
            'active' => $this->faker->boolean(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->firstName(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail(),
            'timezone' => $this->faker->timezone(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
