<?php

namespace App\Http\ApiV1\Modules\Users\Filters;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersUserFullName implements Filter
{
    public function __invoke(Builder $query, $value, string $property): void
    {
        $query->whereRaw("CONCAT(last_name, ' ' , first_name, ' ', middle_name) LIKE '%$value%'");
    }
}
