<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Http\ApiV1\Support\Rules\StrictEmail;
use Illuminate\Validation\Rule;

class CreateUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'login' => ['required', Rule::unique(User::class)],
            'active' => ['boolean'],
            'password' => ['nullable'],
            'first_name' => ['nullable'],
            'last_name' => ['nullable'],
            'middle_name' => ['nullable'],
            'timezone' => ['required', 'timezone'],
            'phone' => ['required', 'regex:/^\+7\d{10}$/', Rule::unique(User::class)],
            'email' => ['required', Rule::unique(User::class), new StrictEmail()],
        ];
    }
}
