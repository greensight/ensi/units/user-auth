<?php

namespace App\Http\ApiV1\Modules\Users\Resources;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Role */
class RolesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'active' => $this->active,
            'rights_access' => $this->rights_access,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'expires' => $this->whenPivotLoaded('role_user', function () {
                return $this->pivot->expires;
            }),
        ];
    }
}
