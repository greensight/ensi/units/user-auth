<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Domain\Users\Models\RightsAccess;
use App\Http\ApiV1\Modules\Users\Resources\RightsAccessResource;
use Illuminate\Contracts\Support\Responsable;

class EnumsController
{
    public function getRightsAccess(): Responsable
    {
        return RightsAccessResource::collection(RightsAccess::all());
    }
}
