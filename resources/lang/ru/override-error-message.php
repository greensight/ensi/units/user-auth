<?php

use Laravel\Passport\Exceptions\OAuthServerException;
use League\OAuth2\Server\Exception\OAuthServerException as LeagueOAuthServerException;

return [
    OAuthServerException::class => [
        6 => 'Неверный логин или пароль',
        1000 => 'Данная учётная запись неактивна',
        1001 => 'Нет привязанных активных ролей',
    ],
    LeagueOAuthServerException::class => [
        6 => 'Неверный логин или пароль',
        1000 => 'Данная учётная запись неактивна',
        1001 => 'Нет привязанных активных ролей',
    ],
];
